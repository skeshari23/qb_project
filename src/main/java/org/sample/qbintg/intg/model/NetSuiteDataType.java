package org.sample.qbintg.intg.model;

import static org.sample.qbintg.intg.model.MessageEnum.*;
import static org.sample.qbintg.intg.model.DataQuery.*;

public enum NetSuiteDataType {

	NS_BILL(SUCCESSFUL_NS_DATA_RETRIEVEL,RETRIEVE_NS_BILL_ERROR,"VendorBill"),
	NS_BILL_PAYMENT(SUCCESSFUL_NS_DATA_RETRIEVEL,RETRIEVE_NS_BILLPAYMENT_ERROR,"VendorPayment"),
	NS_VENDOR(SUCCESSFUL_NS_DATA_RETRIEVEL,RETRIEVE_NS_VENDOR_ERROR,"Vendor");

	private final MessageEnum success;
	private final MessageEnum failure;
	private final String recordName;
	
	public MessageEnum getFailureMessage(){
		return failure;
	}
	
	public MessageEnum getSuccessMessage(){
		return success;
	}
	
	public String getRecordName(){
		return recordName;
	}
	
	private NetSuiteDataType(MessageEnum success, MessageEnum failure, String rcName){
		this.success = success;
		this.failure = failure;
		this.recordName = rcName;
	}
}

