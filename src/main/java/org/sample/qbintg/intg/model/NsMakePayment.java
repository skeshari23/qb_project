package org.sample.qbintg.intg.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NsMakePayment implements Serializable{

	//private String billPaymentId; //id
	
	

	private String type;
	private String vendorName; 
//	private String txnDate;
//	private Integer account;
	private String amountPaid; 
	private String billTxnId;
	private String notes;
	private String currency;
	private String exchangerate;
	
	
	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	@JsonProperty("exchangerate")
	public String getExchangerate() {
		return exchangerate;
	}

	@JsonProperty("memo")
	public String getNotes() {
		return notes;
	}


	@JsonProperty("entityname")
	public String getVendorName() {
		return vendorName;
	}

	/*@JsonProperty("id")
	public String getBillPaymentId() {
		return billPaymentId;
	}
*/
	

	@JsonProperty("amountpaid")
	public String getAmountPaid() {
		return amountPaid;
	}

	@JsonProperty("transactionnumber")
	public String getBillTxnId() {
		return billTxnId;
	}


	/*@JsonProperty("trandate")
	public String getTxnDate() {
		return txnDate;
	}*/
	/*@JsonProperty("account")
	public Integer getAccount() {
		return account;
	}

	public void setAccount(Integer account) {
		this.account = account;
	}*/

	@JsonProperty("recordtype")
	public String getType() {
		return type;
	}


/*	@Override
	public String toString() {
		return "Data [vendorName=" + vendorName + ", txnDate=" + txnDate + ", paymentType=" + paymentType
				+ ", amountPaid=" + amountPaid + ", billTxnId=" + billTxnId + "]";
	}*/


	public void setType(String type) {
		this.type = type;
	}
	

	/*public void setBillPaymentId(String billPaymentId) {
		this.billPaymentId = billPaymentId;
	}

*/
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setExchangerate(String exchangerate) {
		this.exchangerate = exchangerate;
	}

/*	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

*/

	public void setAmountPaid(String amountPaid) {
		this.amountPaid = amountPaid;
	}


	public void setBillTxnId(String billTxnId) {
		this.billTxnId = billTxnId;
	}




	
	
	
	
}
