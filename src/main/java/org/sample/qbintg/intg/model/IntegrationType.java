package org.sample.qbintg.intg.model;

public enum IntegrationType {

	NETSUITE,
	QUICKBOOKS,
	XERO;

}

