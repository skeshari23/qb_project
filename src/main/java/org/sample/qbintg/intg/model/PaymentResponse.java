package org.sample.qbintg.intg.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PaymentResponse implements Serializable {

	@JsonIgnore
	private static final long serialVersionUID  =1L;
	
	
	private String paymentDate;
	private String status;
	private String notes;
	private List<String> errors;
	private String UUID;
	private Integer squenceNum;
	
	public Integer getSquenceNum() {
		return squenceNum;
	}
	public void setSquenceNum(Integer squenceNum) {
		this.squenceNum = squenceNum;
	}
	public String getUUID() {
		return UUID;
	}
	public void setUUID(String uUID) {
		UUID = uUID;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	
}
