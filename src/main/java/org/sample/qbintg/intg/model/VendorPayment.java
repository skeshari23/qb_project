package org.sample.qbintg.intg.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class VendorPayment  implements Serializable{
	@JsonIgnore
	private static final long serialVersionUID  =1L;
	private String uuid;
	private String accountNumber;
	private String routungNumber;
	private String accountName;

	private BigDecimal amount;
	private String currency;
	private String accountType;
	private String notes;
	private String vendorId;
	private String vendorName;
	private String initiatorUserId;

	
	

	@Override
	public String toString() {
		return "VendorPayment [uuid=" + uuid + ", accountNumber=" + accountNumber + ", routungNumber=" + routungNumber
				+ ", accountName=" + accountName + ", amount=" + amount + ", currency=" + currency + ", accountType="
				+ accountType + ", notes=" + notes + ", vendorId=" + vendorId + ", vendorName=" + vendorName
				+ ", initiatorUserId=" + initiatorUserId + "]";
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getInitiatorUserId() {
		return initiatorUserId;
	}
	public void setInitiatorUserId(String initiatorUserId) {
		this.initiatorUserId = initiatorUserId;
	}


	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}


	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getRoutungNumber() {
		return routungNumber;
	}
	public void setRoutungNumber(String routungNumber) {
		this.routungNumber = routungNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}

}
