package org.sample.qbintg.intg.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public enum PaymentStatusEnum {

	SUCCESSFULL("Succesfull"), FAILED("Failed"), IN_PROGRESS("In Progress"), VOIDED("Voided");

	private final String val;
	PaymentStatusEnum(String v) {
		val = v;
	}
	public String val() {
		return val;
	}

	private static final List<PaymentStatusEnum> VALUES =  Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	public static PaymentStatusEnum randomPaymentStat()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
