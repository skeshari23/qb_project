package org.sample.qbintg.intg.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.sample.qbintg.intg.model.NsMakePayment;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.utils.StreamUtils;
import com.intuit.ipp.data.BillPaymentTypeEnum;


@RestController
public class NetSuiteOAuthController {
	private static final Logger logger = Logger.getLogger(NetSuiteOAuthController.class);
	private final AtomicInteger counter = new AtomicInteger();


	private static final String CONSUMER_KEY = "e3f3f58db0aaf3665f6d1cd9b0c63638b786ec5c060b2408828a6b05c2db221a";
	private static final String CONSUMER_SECRET = "4c3a1f3bc421d0ff0a975541aaf111d15f7a36e26e51c0a744df59368a6d4e6b";
	private static final String TOKEN_KEY = "1d41d7c17bad9d547ff863f411921e58bc95aee5a2faa6f6654b81fd394fedd4";
	private static final String TOKEN_SECRET = "0d6d3a40f0a6de4ccd1f732d63038575dbd36a9f7c061166ac0400d495aec3d2";
	private static final String baseUrl = "https://rest.netsuite.com/app/site/hosting/restlet.nl?";
	public static void main(String[] args) throws Exception {
	//	getVendors(null);
		doTbaCall();
		/*System.out.println("Test: NetSuite Token Based Authentication...");
		*/
		System.out.println("Test: Done!");
	}


	/*@ResponseBody
	@RequestMapping("/getNetSuiteBills")
	public String getBills(HttpSession session) {
		return getNetSuiteData(session,"Bill");
	}


	@ResponseBody
	@RequestMapping("/getNetSuiteBillPayments")
	public String getBillPayments(HttpSession session) {
		return getNetSuiteData(session,"BillPayments");
	}*/


	private final static  int scriptNum = 584;
	private   final static  int deployNum =2;


	/*@ResponseBody
	@RequestMapping("/getNetSuiteVendors")*/
	private static ObjectMapper mapper = new ObjectMapper();
	public static String getVendors(HttpSession session) {
		OAuthConfig authConfig = new OAuthConfig(CONSUMER_KEY, CONSUMER_SECRET);
		OAuth1AccessToken token = new OAuth1AccessToken(TOKEN_KEY, TOKEN_SECRET);
		//String vendorBillsStr = getNetSuiteData(authConfig, token,"VendorBill",0,true);
		String billPayments = getNetSuiteData(authConfig, token,"VendorPayment",0,true);

		//recordType: ? recordFilter: , isMultiple=false?
		//String vendorBills = getNetSuiteData(authConfig, token,"VendorBill",2143);

		/*		String pncPaySetup = getNetSuiteData(authConfig, token,"customrecord_pncpay_setup", 7,false);
		String vendors = getNetSuiteData(authConfig, token,"Vendor",38);
		String billPayments = getNetSuiteData(authConfig, token,"VendorPayment",7615);
		 */		//getBills 
		//for each bill, date, payee, category, due date, balance, total, action
		return "Done";
	}


	private static String getNetSuiteData(OAuthConfig authConfig,OAuth1AccessToken token, String recordType, Integer id,boolean isMultiple){
		String url  = baseUrl+"script="+scriptNum+"&deploy="+deployNum+"&recordtype="+recordType+"&id="+id+"&isMultiple="+isMultiple;
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.GET, url);
			request.setRealm("TSTDRV1798497");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");
			auth10aService.signRequest(token, request);
			Response response = auth10aService.execute(request);
			String body = StreamUtils.getStreamContents(response.getStream());
			System.out.println(body);
			return body;
		}
		catch(Exception e) {
			System.err.println("Error Calling API"+e.getMessage());
			e.printStackTrace();
		}
		return "Exception occured for "+recordType;
	}


	/*// consumes="application/json"
	@RequestMapping(value = "/payVendor", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<PaymentResponse> payVendor(@RequestBody VendorPayment vendorPayment) {
		System.out.println("Inside vendor method");
		logger.info("Inside payVendor method");
		if(vendorPayment==null) {
			logger.error("Null Request");
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		logger.info(""+vendorPayment.toString());
		PaymentResponse paymentResponse = new PaymentResponse();
		paymentResponse.setSquenceNum(counter.incrementAndGet());
		paymentResponse.setPaymentDate(new DateTime().toString());

		if(vendorPayment.getUuid()==null || vendorPayment.getAccountNumber()==null 
				|| vendorPayment.getRoutungNumber()==null || vendorPayment.getAccountType()==null || vendorPayment.getAccountName()==null || vendorPayment.getCurrency()==null
				|| vendorPayment.getAmount()==null ||  vendorPayment.getAmount().compareTo(BigDecimal.ZERO)<=0) {
			paymentResponse.setStatus(PaymentStatusEnum.FAILED.val());
			paymentResponse.setErrors(Arrays.asList(new String[]{"Either of the mandatory fields are not valid"}));
			paymentResponse.setPaymentDate(new DateTime().toString());
			return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);
		}
		// call payment service
		paymentResponse.setUUID(vendorPayment.getUuid());
		paymentResponse.setStatus(PaymentStatusEnum.IN_PROGRESS.val());
		paymentResponse.setNotes("Recieved the Payment Request for vendor: "+ vendorPayment.getVendorName()+" for payment of  : "+ vendorPayment.getCurrency().toUpperCase()+" "+vendorPayment.getAmount());
		return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);
	}


	@RequestMapping(value = "/paymentstatus", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<PaymentResponse>> getStatus() {
		logger.info("Inside Payment Status method");
		PaymentResponse paymentResponse = new PaymentResponse();
		paymentResponse.setSquenceNum(counter.incrementAndGet());
		paymentResponse.setPaymentDate(new DateTime().toString());
		paymentResponse.setUUID("698cf38c-ba8a-41f2-a5ce-0894b803a98b");
		paymentResponse.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse.setNotes("This is sample notes. Payment is  "+ paymentResponse.getStatus());


		PaymentResponse paymentResponse2 = new PaymentResponse();
		paymentResponse2.setSquenceNum(counter.incrementAndGet());
		paymentResponse2.setPaymentDate(new DateTime().toString());
		paymentResponse2.setUUID("2ad9dd4e-aa17-4053-b089-5f5ebcc01614");
		paymentResponse2.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse2.setNotes("This is sample notes. Payment is  "+ paymentResponse2.getStatus());


		PaymentResponse paymentResponse3 = new PaymentResponse();
		paymentResponse3.setSquenceNum(counter.incrementAndGet());
		paymentResponse3.setPaymentDate(new DateTime().toString());
		paymentResponse3.setUUID("b402d453-bd7b-4c68-a3ec-4d605dcc5b04");
		paymentResponse3.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse3.setNotes("This is sample notes. Payment is  "+ paymentResponse3.getStatus());

		PaymentResponse paymentResponse4 = new PaymentResponse();
		paymentResponse4.setSquenceNum(counter.incrementAndGet());
		paymentResponse4.setPaymentDate(new DateTime().toString());
		paymentResponse4.setUUID("436594bf-a664-44f2-aabc-b254a27412ed");
		paymentResponse4.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse4.setNotes("This is sample notes. Payment is  "+ paymentResponse4.getStatus());
		List<PaymentResponse> paymentResponses = new ArrayList<PaymentResponse>();
		paymentResponses.add(paymentResponse);
		paymentResponses.add(paymentResponse2);
		paymentResponses.add(paymentResponse3);
		paymentResponses.add(paymentResponse4);
		return new ResponseEntity<List<PaymentResponse>>(paymentResponses,HttpStatus.OK);
	}

	@RequestMapping(value = "/paymentstatus/{uuid}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<PaymentResponse> getStatus(@PathVariable("uuid")  String uuid) {
		logger.info("Inside Payment Status method for particular transaction");
		if(StringUtils.isEmpty(uuid)) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		PaymentResponse paymentResponse = new PaymentResponse();
		paymentResponse.setSquenceNum(counter.incrementAndGet());
		paymentResponse.setPaymentDate(new DateTime().toString());
		paymentResponse.setUUID(uuid);
		paymentResponse.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse.setNotes("This is sample notes. Payment is  "+ paymentResponse.getStatus());
		return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);
	}
	 */


	private static void doGetCall() throws Exception {
		/*final String url = "https://rest.netsuite.com/app/site/hosting/restlet.nl?script=583&deploy=1";
		final OAuth10aService service = new ServiceBuilder(CONSUMER_KEY).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
		final OAuth1RequestToken requestToken = service.getRequestToken();
		String authUrl = service.getAuthorizationUrl(requestToken);
		final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, "verifier you got from the user/callback");
		final OAuthRequest request = new OAuthRequest(Verb.GET, url);
		service.signRequest(accessToken, request); // the access token from step 4
		final Response response = service.execute(request);
		System.out.println(response.getBody());*/
		int scriptNum = 584;
		int deployNum =2;
		String recordType = "customrecord_pncpay_setup";
		int id = 7;

		String pncSetupUrl = baseUrl+"script="+scriptNum+"&deploy="+deployNum+"&recordtype="+recordType+"&id="+id;

		recordType = "VendorBill";
		id =10;
		String billsUrl = baseUrl+"script="+scriptNum+"&deploy="+deployNum+"&recordtype="+recordType+"&id="+id;


		//getBills 
		//for each bill, date, payee, category, due date, balance, total, action


		String url = billsUrl;
		OAuthConfig authConfig = new OAuthConfig(CONSUMER_KEY, CONSUMER_SECRET);
		OAuth1AccessToken token = new OAuth1AccessToken(TOKEN_KEY, TOKEN_SECRET);
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.GET, url);
			request.setRealm("TSTDRV1798497");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");
			auth10aService.signRequest(token, request);
			System.out.println("Result:");
			Response response = auth10aService.execute(request);
			String body = StreamUtils.getStreamContents(response.getStream());
			System.out.println(body);
		}
		catch(Exception e) {
			System.err.println("Error Calling API"+e.getMessage());
			e.printStackTrace();
		}

	}
	
	static class NSApi extends DefaultApi10a {

		@Override
		public String getRequestTokenEndpoint() {
			return null;
		}

		@Override
		public String getAccessTokenEndpoint() {
			return null;
		}

		@Override
		public String getAuthorizationUrl(OAuth1RequestToken requestToken) {
			return null;
		}

	}
	private static void doTbaCall() throws Exception {
		final String url = "https://rest.netsuite.com/app/site/hosting/restlet.nl?script=584&deploy=2";


		OAuthConfig authConfig = new OAuthConfig(CONSUMER_KEY, CONSUMER_SECRET);
		OAuth1AccessToken token = new OAuth1AccessToken(TOKEN_KEY, TOKEN_SECRET);
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.POST, url);
			request.setRealm("TSTDRV1798497");
			request.addHeader("Content-Type", "application/json");
			NsMakePayment nsPayObj = new NsMakePayment();
			nsPayObj.setAmountPaid("440.00");
			//nsPayObj.setTxnDate("02/02/2018");
			//nsPayObj.setAccount(1);
			nsPayObj.setNotes("Data Found In The System");
			nsPayObj.setVendorName("Nikon");
			nsPayObj.setExchangerate("1.00");
			nsPayObj.setCurrency("1");
			nsPayObj.setType("VendorPayment");
			String jsonInString = mapper.writeValueAsString(nsPayObj);
			request.setPayload(jsonInString);
			System.out.println(request.getStringPayload());
			auth10aService.signRequest(token, request);
			System.out.println("RESULT:");
			Response response = auth10aService.execute(request);
			String body = StreamUtils.getStreamContents(response.getStream());
			System.out.println(body);
			logger.info("");
		}
		catch(Exception e) {
			System.err.println("Error Calling API"+e.getMessage());
			e.printStackTrace();
		}

	}

	
}