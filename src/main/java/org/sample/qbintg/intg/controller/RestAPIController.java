package org.sample.qbintg.intg.controller;

import static org.sample.qbintg.intg.model.MessageEnum.BEARER_ERROR;
import static org.sample.qbintg.intg.model.MessageEnum.CONNECTION_INCOMPLETE;
import static org.sample.qbintg.intg.model.MessageEnum.GENERIC_ERROR;
import static org.sample.qbintg.intg.model.MessageEnum.REFRESH_ERROR;
import static org.sample.qbintg.intg.model.MessageEnum.SETUP_ERROR;
import static org.sample.qbintg.intg.model.MessageEnum.UNAUTH_ERROR;
import static org.sample.qbintg.intg.model.NetSuiteDataType.NS_BILL;
import static org.sample.qbintg.intg.model.NetSuiteDataType.NS_BILL_PAYMENT;
import static org.sample.qbintg.intg.model.NetSuiteDataType.NS_VENDOR;
import static org.sample.qbintg.intg.model.QuickBooksDataType.BILL;
import static org.sample.qbintg.intg.model.QuickBooksDataType.BILL_PAYMENT;
import static org.sample.qbintg.intg.model.QuickBooksDataType.VENDOR;
import static org.sample.qbintg.intg.util.Utilities.getMessage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.sample.qbintg.OAuth2PlatformClientFactory;
import org.sample.qbintg.intg.constant.PaymentStatusEnum;
import org.sample.qbintg.intg.controller.NetSuiteOAuthController.NSApi;
import org.sample.qbintg.intg.helper.DataHelper;
import org.sample.qbintg.intg.model.MakePaymentModalView;
import org.sample.qbintg.intg.model.MessageEnum;
import org.sample.qbintg.intg.model.NetSuiteDataType;
import org.sample.qbintg.intg.model.NsMakePayment;
import org.sample.qbintg.intg.model.PaymentResponse;
import org.sample.qbintg.intg.model.QuickBooksDataType;
import org.sample.qbintg.intg.model.VendorDto;
import org.sample.qbintg.intg.model.VendorPayment;
import org.sample.qbintg.repository.IDataRepository;
import org.sample.qbintg.repository.VendorDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.utils.StreamUtils;
import com.intuit.ipp.core.Context;
import com.intuit.ipp.core.ServiceType;
import com.intuit.ipp.data.BillPaymentTypeEnum;
import com.intuit.ipp.data.Error;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.exception.InvalidTokenException;
import com.intuit.ipp.security.OAuth2Authorizer;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.QueryResult;
import com.intuit.ipp.util.Config;
import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.data.BearerTokenResponse;
import com.intuit.oauth2.exception.OAuthException;

@RestController
public class RestAPIController {

	private static final Logger logger = Logger.getLogger(RestAPIController.class);

	private final AtomicInteger counter = new AtomicInteger();
	@Autowired
	private OAuth2PlatformClientFactory factory;

	@Autowired
	private VendorDataRepository vendorDataRepository;

	private DataService dataService;

	@Autowired
	private Environment env;

	@RequestMapping("/health")
	public String checkHealth(){
		return "Working fine";
	}


	//Throttling can be implemented here
	@ResponseBody
	@RequestMapping("/getBills")
	public String getBills(HttpSession session) {
		String quickBooksData =  getData(session, BILL);
		String netSuiteData = getNetSuiteData(NS_BILL);
		String returnStr = "{\"quickbooks\":"+quickBooksData+",\"netsuite\":"+netSuiteData+"}";
		return returnStr;
	}


	@ResponseBody
	@RequestMapping("/getBillPayments")
	public String getBillPayments(HttpSession session) {
		String quickBooksData =  getData(session, BILL_PAYMENT);
		String netSuiteData = getNetSuiteData(NS_BILL_PAYMENT);
		String returnStr = "{\"quickbooks\":"+quickBooksData+",\"netsuite\":"+netSuiteData+"}";
		return returnStr;
	}


	@ResponseBody
	@RequestMapping("/getVendors")
	public String getVendors(HttpSession session) {
		String quickBooksData =  getData(session, VENDOR);
		String netSuiteData = getNetSuiteData(NS_VENDOR);
		String returnStr = "{\"quickbooks\":"+quickBooksData+",\"netsuite\":"+netSuiteData+"}";
		return returnStr;
	}



	private final static  int scriptNum = 584;
	private   final static  int deployNum =2;

	private  String getNetSuiteData(NetSuiteDataType dataType){
		MessageEnum failureMessage = dataType.getFailureMessage();
		OAuthConfig authConfig = new OAuthConfig(env.getProperty("netsuite.consumer.key"), env.getProperty("netsuite.consumer.secret"));
		OAuth1AccessToken token = new OAuth1AccessToken(env.getProperty("netsuite.token.key"), env.getProperty("netsuite.token.secret"));
		String url  = env.getProperty("netsuite.baseurl")+"script="+scriptNum+"&deploy="+deployNum+"&recordtype="+dataType.getRecordName()+"&id="+0+"&isMultiple="+true;
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.GET, url);
			request.setRealm("TSTDRV1798497");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");
			auth10aService.signRequest(token, request);
			Response response = auth10aService.execute(request);
			String body = StreamUtils.getStreamContents(response.getStream());
			logger.info(body);
			return  DataHelper.processNSResponseForType(body,dataType);
		}
		catch(Exception e) {
			logger.error("Error Calling NetSuite API",e);
		}
		return getMessage(failureMessage);
	}


	class NSApi extends DefaultApi10a {

		@Override
		public String getRequestTokenEndpoint() {
			return null;
		}

		@Override
		public String getAccessTokenEndpoint() {
			return null;
		}

		@Override
		public String getAuthorizationUrl(OAuth1RequestToken requestToken) {
			return null;
		}

	}




	@ResponseBody
	@RequestMapping(value = "/makePayment", method = RequestMethod.POST)
	public String makePayment(HttpSession session, @RequestBody MakePaymentModalView makePayment) {
		logger.info("Inside Payment object");
		String failureMsg = validateConfig(session);
		boolean isPaid = false;
		if(!StringUtils.isEmpty(failureMsg)){
			return failureMsg;
		}
		try{
			if(makePayment!=null && makePayment.getSource()!=null && makePayment.getSource().equalsIgnoreCase("NetSuite")) {
				isPaid = createNsBillPayment(makePayment);
			}
			else {
				isPaid= DataHelper.createBillPayment(dataService,makePayment);

			}			}
		catch(Exception e){
			return MessageEnum.UNABLE_PAYMENT_ERROR.desc();
		}
		logger.info("Bill Paid : " + isPaid);
		//session.setAttribute("billUpdated", true);
		return "Payment Completed";
	}

	public boolean createNsBillPayment(MakePaymentModalView makePayment) {
		OAuthConfig authConfig = new OAuthConfig(env.getProperty("netsuite.consumer.key"), env.getProperty("netsuite.consumer.secret"));
		OAuth1AccessToken token = new OAuth1AccessToken(env.getProperty("netsuite.token.key"), env.getProperty("netsuite.token.secret"));
		String url  = env.getProperty("netsuite.baseurl")+"script="+scriptNum+"&deploy="+deployNum;
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.POST, url);
			request.setRealm("TSTDRV1798497");

			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");



			NsMakePayment nsPayObj = new NsMakePayment();
			nsPayObj.setAmountPaid(makePayment.getPaymentAmount().toString());
			nsPayObj.setTxnDate(makePayment.getPaymentDate().toString());
			nsPayObj.setPaymentType( BillPaymentTypeEnum.CHECK.toString());
			nsPayObj.setNotes(makePayment.getNote());
			nsPayObj.setVendorName(makePayment.getVendorId());
			nsPayObj.setExchangerate("1.00");
			nsPayObj.setCurrency("1");

			/*// add bill payment with minimum mandatory fields
			payment.setPaymentAmount(billObj.getBalance());
			billPayment.setVendorRef(billObj.getVendorRef());
			billPayment.setTxnDate(payment.getPaymentDate());
			billPayment.setPrivateNote(payment.getNote());
			billPayment.setTotalAmt(payment.getPaymentAmount());
			BillPaymentTypeEnum paymentType = BillPaymentTypeEnum.CHECK; //TODO harcoded
			if(paymentType.equals(BillPaymentTypeEnum.CHECK)){
				billPayment.setCheckPayment(getCheckData(service));
			}
			else{
				billPayment.setCreditCardPayment(getCreditCardData(service));
			}
			billPayment.setPayType(paymentType);
			//linked transaction for bill 
			 */			request.setPayload("{\"message\" : \"Hello World\"}"); //"&recordtype="+NS_BILL_PAYMENT.getRecordName()+"&id="+0+"&isMultiple="+true;
			 System.out.println("PAYLOAD:");
			 System.out.println(request.getStringPayload());
			 auth10aService.signRequest(token, request);
			 Response response = auth10aService.execute(request);
			 String body = StreamUtils.getStreamContents(response.getStream());
			 logger.info(body);
			 return true;
		}
		catch(Exception e) {
			logger.error(MessageEnum.UNABLE_PAYMENT_ERROR.desc() +e.getMessage());
			return false;
		}



		/*var vendorBillPayment = record.create({
            type: record.Type.VENDOR_PAYMENT,
            isDynamic: false,
            defaultValues: {
                entity: 45
            }
        })

        vendorBillPayment.setValue({
            fieldId: 'entityname',
            value: "Superior ISP"
        })

        vendorBillPayment.setValue({
            fieldId: 'account',
            value: 129
        })

        vendorBillPayment.setValue({
            fieldId: 'currency',
            value: 1
        })

        vendorBillPayment.setValue({
            fieldId: 'customform',
            value: 45
        })

        vendorBillPayment.setValue({
            fieldId: 'exchangerate',
            value: "1.00"
        })

        var recordId = vendorBillPayment.save({
             enableSourcing: false,
             ignoreMandatoryFields: true
        })*/
	}

	@ResponseBody
	@RequestMapping(value = "/saveVendor", method = RequestMethod.POST)
	public String saveVendor(HttpSession session, @RequestBody VendorDto vendorDto) {
		logger.info("Inside Vendor save method ");
		String failureMsg = validateConfig(session);
		boolean isSaved = false;
		if(!StringUtils.isEmpty(failureMsg)){
			return failureMsg;
		}
		try{
			isSaved= DataHelper.saveVendor(dataService,vendorDataRepository,vendorDto);
		}
		catch(Exception e){
			return MessageEnum.UNABLE_PAYMENT_ERROR.desc();
		}
		logger.info("Vendor Saved " + isSaved);
		//session.setAttribute("billUpdated", true);
		return "Payment Completed";
	}


	// consumes="application/json"
	@RequestMapping(value = "/payVendor", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<PaymentResponse> payVendor(@RequestBody VendorPayment vendorPayment) {
		System.out.println("Inside vendor method");
		logger.info("Inside payVendor method");
		if(vendorPayment==null) {
			logger.error("Null Request");
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		logger.info(""+vendorPayment.toString());
		PaymentResponse paymentResponse = new PaymentResponse();
		paymentResponse.setSquenceNum(counter.incrementAndGet());
		paymentResponse.setPaymentDate(new DateTime().toString());

		if(vendorPayment.getUuid()==null || vendorPayment.getAccountNumber()==null 
				|| vendorPayment.getRoutungNumber()==null || vendorPayment.getAccountType()==null || vendorPayment.getAccountName()==null || vendorPayment.getCurrency()==null
				|| vendorPayment.getAmount()==null ||  vendorPayment.getAmount().compareTo(BigDecimal.ZERO)<=0) {
			paymentResponse.setStatus(PaymentStatusEnum.FAILED.val());
			paymentResponse.setErrors(Arrays.asList(new String[]{"Either of the mandatory fields are not valid"}));
			paymentResponse.setPaymentDate(new DateTime().toString());
			return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);
		}
		// call payment service
		paymentResponse.setUUID(vendorPayment.getUuid());
		paymentResponse.setStatus(PaymentStatusEnum.IN_PROGRESS.val());
		paymentResponse.setNotes("Recieved the Payment Request for vendor: "+ vendorPayment.getVendorName()+" for payment of  : "+ vendorPayment.getCurrency().toUpperCase()+" "+vendorPayment.getAmount());
		return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);
	}


	@RequestMapping(value = "/paymentstatus", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<PaymentResponse>> getStatus() {
		logger.info("Inside Payment Status method");
		PaymentResponse paymentResponse = new PaymentResponse();
		paymentResponse.setSquenceNum(counter.incrementAndGet());
		paymentResponse.setPaymentDate(new DateTime().toString());
		paymentResponse.setUUID("698cf38c-ba8a-41f2-a5ce-0894b803a98b");
		paymentResponse.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse.setNotes("This is sample notes. Payment is  "+ paymentResponse.getStatus());


		PaymentResponse paymentResponse2 = new PaymentResponse();
		paymentResponse2.setSquenceNum(counter.incrementAndGet());
		paymentResponse2.setPaymentDate(new DateTime().toString());
		paymentResponse2.setUUID("2ad9dd4e-aa17-4053-b089-5f5ebcc01614");
		paymentResponse2.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse2.setNotes("This is sample notes. Payment is  "+ paymentResponse2.getStatus());


		PaymentResponse paymentResponse3 = new PaymentResponse();
		paymentResponse3.setSquenceNum(counter.incrementAndGet());
		paymentResponse3.setPaymentDate(new DateTime().toString());
		paymentResponse3.setUUID("b402d453-bd7b-4c68-a3ec-4d605dcc5b04");
		paymentResponse3.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse3.setNotes("This is sample notes. Payment is  "+ paymentResponse3.getStatus());

		PaymentResponse paymentResponse4 = new PaymentResponse();
		paymentResponse4.setSquenceNum(counter.incrementAndGet());
		paymentResponse4.setPaymentDate(new DateTime().toString());
		paymentResponse4.setUUID("436594bf-a664-44f2-aabc-b254a27412ed");
		paymentResponse4.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse4.setNotes("This is sample notes. Payment is  "+ paymentResponse4.getStatus());
		List<PaymentResponse> paymentResponses = new ArrayList<PaymentResponse>();
		paymentResponses.add(paymentResponse);
		paymentResponses.add(paymentResponse2);
		paymentResponses.add(paymentResponse3);
		paymentResponses.add(paymentResponse4);
		return new ResponseEntity<List<PaymentResponse>>(paymentResponses,HttpStatus.OK);
	}

	@RequestMapping(value = "/paymentstatus/{uuid}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<PaymentResponse> getStatus(@PathVariable("uuid")  String uuid) {
		logger.info("Inside Payment Status method for particular transaction");
		if(StringUtils.isEmpty(uuid)) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		PaymentResponse paymentResponse = new PaymentResponse();
		paymentResponse.setSquenceNum(counter.incrementAndGet());
		paymentResponse.setPaymentDate(new DateTime().toString());
		paymentResponse.setUUID(uuid);
		paymentResponse.setStatus(PaymentStatusEnum.randomPaymentStat().val());
		paymentResponse.setNotes("This is sample notes. Payment is  "+ paymentResponse.getStatus());
		return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);
	}






	private String validateConfig(HttpSession session){
		String failureMsg="";
		try {
			String realmId = (String)session.getAttribute("realmId");
			if (StringUtils.isEmpty(realmId)) {
				failureMsg =getMessage(CONNECTION_INCOMPLETE);
			}
			String accessToken = (String)session.getAttribute("access_token");
			String url = factory.getPropertyValue("IntuitAccountingAPIHost") + "/v3/company";
			Config.setProperty(Config.BASE_URL_QBO, url);
			getDataService(realmId, accessToken);
		}
		catch (FMSException e) {
			List<Error> list = e.getErrorList();
			list.forEach(error -> logger.error(SETUP_ERROR.desc() + error.getMessage()));
			return getMessage(CONNECTION_INCOMPLETE);
		}
		catch(Exception e){
			logger.error(GENERIC_ERROR.desc(),e);
			return getMessage(GENERIC_ERROR);
		}
		return failureMsg;
	}


	private void getDataService(String realmId, String accessToken) throws FMSException {
		OAuth2Authorizer oauth = new OAuth2Authorizer(accessToken);
		Context context = new Context(oauth, ServiceType.QBO, realmId); 
		dataService =  new DataService(context);
	}


	/*
	 * This method is called to handle 401 status code - 
	 * If a 401 response is received, refresh tokens should be used to get a new access token,
	 * and the API call should be tried again.
	 */

	private String  refreshTokens(HttpSession session) throws Exception {
		try{
			OAuth2PlatformClient client  = factory.getOAuth2PlatformClient();
			String refreshToken = (String)session.getAttribute("refresh_token");
			BearerTokenResponse bearerTokenResponse = client.refreshToken(refreshToken);
			session.setAttribute("access_token", bearerTokenResponse.getAccessToken());
			session.setAttribute("refresh_token", bearerTokenResponse.getRefreshToken());
			getDataService((String)session.getAttribute("realmId"), bearerTokenResponse.getAccessToken());
			return "";
		}
		catch (OAuthException e1) {
			logger.error(BEARER_ERROR.desc() + e1.getMessage());
			return getMessage(REFRESH_ERROR);
		}
		catch (FMSException e) {
			List<Error> list = e.getErrorList();
			list.forEach(error -> logger.error(SETUP_ERROR.desc() + error.getMessage()));
			return getMessage(CONNECTION_INCOMPLETE);
		}

	}



	private String getData(HttpSession session, QuickBooksDataType dataType){
		MessageEnum messageEnum = dataType.getFailureMessage();
		String failureMsg=messageEnum.desc();
		String sql = dataType.getSql();
		IDataRepository dataRepository = null;
		if(dataType==VENDOR){
			dataRepository = vendorDataRepository;
		}

		try {
			failureMsg = validateConfig(session);
			if(!StringUtils.isEmpty(failureMsg)){
				return failureMsg;
			}
			QueryResult queryResult = dataService.executeQuery(sql);
			return DataHelper.processResponseForType(queryResult,dataType,dataRepository);
		}
		catch (InvalidTokenException e) {			
			logger.error(failureMsg + e.getMessage());
			logger.info(UNAUTH_ERROR.desc());
			try {
				refreshTokens(session);
				logger.info("Calling data service again");
				QueryResult queryResult = dataService.executeQuery(sql);
				return  DataHelper.processResponseForType(queryResult,dataType,dataRepository);
			} 
			catch (FMSException e2) {
				logger.error(failureMsg + e2.getMessage());
				return getMessage(messageEnum);
			}
			catch (Exception e1) {
				logger.error("Retry failed with fresh token" + e.getMessage());
				return getMessage(messageEnum);
			}

		} 
		catch (FMSException e2) {
			logger.error(failureMsg + e2.getMessage());
			return getMessage(messageEnum);
		}
		catch (Exception e1) {
			logger.error(failureMsg + e1.getMessage());
			return getMessage(messageEnum);
		}
	}










}
